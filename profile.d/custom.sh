#! /usr/bin/env bash

if [ "$EUID" = "0" ] || [ "$USER" = "root" ] ; then
    export PS1='\n\[\033[01;31m\][\u@\h] \[\033[00m\]\w\n\[\033[01;34m\]#\[\033[00m\] '
else
    export PS1='\n\[\033[01;32m\][\u@\h] \[\033[00m\]\w\n\[\033[01;34m\]\$\[\033[00m\] '
fi

export PATH="/home/user/bin:/home/user/.local/bin:${PATH}"
export INFINALITY_FT_FILTER_PARAMS="0.00 0.35 0.35 0.35 0.00"

# Input method
export XMODIFIERS="@im=none"
export GTK_IM_MODULE=xim
export QT_IM_MODULE=xim

# Java options
export _JAVA_OPTIONS="-Dswing.aatext=true -Dawt.useSystemAAFontSettings=lcd"
# export I_WANT_GLOBAL_JAVA_OPTIONS=true
export SILENCE_JAVA_OPTIONS_WARNING=true

# Golang
#export GOROOT="/home/user/dev/go"
#export PATH="${PATH}:${GOROOT}/bin"
export GOPATH="/home/user/dev/gocode"

alias vi="vim -v -u NONE -c 'set t_Co=0'"

#export PATH="/usr/lib/distcc/bin:${PATH}"

mkcd() {
    DIR="$1"

    if [ ! -d "${DIR}" ]; then
        if [ ! -e "${DIR}" ]; then
            mkdir -pv "${DIR}"
            cd "${DIR}"
        else
            echo "\"${DIR}\" is already exists and is not a directory." >&2
        fi
    else
        cd "${DIR}"
    fi
}

cdls() {
    DIR="$1"

    if [ -d "$DIR" ]; then
        cd "$DIR"
        ls
    fi
}

bashrc() {
    source $HOME/.bashrc
}
