#!/bin/sh
# Default acpi script that takes an entry for all actions

set $*

PATH="$PATH:/home/user/bin"

case "$1" in
    ac_adapter)
        case "$4" in
            00000000)
                cat /tmp/backlight_brightness > /sys/class/backlight/intel_backlight/brightness
                xuser /home/user/bin/adapter-notify 0
            ;;
            00000001)
                cat /tmp/backlight_brightness > /sys/class/backlight/intel_backlight/brightness
                xuser /home/user/bin/adapter-notify 1
            ;;
        esac
    ;;

    video/brightnessup)
        /home/user/bin/brightness up 1
    ;;

    video/brightnessdown)
        /home/user/bin/brightness down 1
    ;;

    jack/headphone)
        jack_notify $3
        case $3 in
            unplug)
                amixer set Master 0% mute -q
                #xuser qdbus org.mpris.clementine /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Pause
            ;;
            plug)
                amixer set Master 60% unmute -q
            ;;
        esac
    ;;

#    button/volumeup)
#        amixer set Master 1%+
#    ;;
#
#    button/volumedown)
#        amixer set Master 1%-
#    ;;
#
#    button/mute)
#        amixer set Master toggle
#    ;;

    button/lid)
        case $3 in
            open)
                echo # xuser /home/user/bin/simulate_activity
            ;;
            close)
                echo
            ;;
        esac
    ;;

    battery)
        if ! grep 1 /sys/class/power_supply/ADP1/online ; then
            CHARGE=`cat /sys/class/power_supply/BAT1/charge_now`
            MAX_CHARGE=`head -n1 /sys/class/power_supply/BAT1/charge_full`
            PERCENT=$(( $CHARGE * 100 / $MAX_CHARGE ))
            if [ "$PERCENT" -le "10" ]; then
                xuser /home/user/bin/my_suspend || /usr/sbin/pm_suspend
            elif [ "$PERCENT" -le "12" ]; then
                xuser /home/user/bin/adapter-notify 4
            elif [ "$PERCENT" -le "15" ]; then
                xuser /home/user/bin/adapter-notify 3
            elif [ "$PERCENT" -le "20" ]; then
                xuser /home/user/bin/adapter-notify 2
            fi
        fi
    ;;



esac

# vim:set ts=4 sw=4 ft=sh et:
